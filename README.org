Welcome to ~follow-the-sun~, a tool for choosing between light and dark theme based on the time of day. It will also automatically change your theme when the sun rises or sets.

* Usage
If you use ~use-package~, you can use a form like this:
#+BEGIN_SRC emacs-lisp
  (use-package follow-the-sun
    :straight (:host gitlab :repo "bricka/emacs-follow-the-sun")
    :config
    (follow-the-sun 'light-theme 'dark-theme)
    )
#+END_SRC

In order for the package to work, you must set ~calendar-latitude~ and ~calendar-longitude~ before requiring the package. This is used by functions like ~solar-sunrise-sunset~ to determine when the sun rises and sets where you are.
