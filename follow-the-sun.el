;;; follow-the-sun.el --- Manage theme depending on the sun.  -*- lexical-binding: t; -*-

;; Author: Alex Figl-Brick <alex@alexbrick.me>

;;; Commentary:

;;; Code:
(require 'cal-dst)
(require 'solar)
(require 'time-date)

(unless (and calendar-longitude calendar-latitude)
  (error "Must define calendar-longitude and calendar-latitude in order to use follow-the-sun")
  )

(defvar follow-the-sun--light-mode-name nil "Light mode.")
(defvar follow-the-sun--dark-mode-name nil "Dark mode.")

(defun follow-the-sun--current-time-in-tz ()
  "Calculate and return the current time in the current TZ, decoded."
   (decode-time (current-time))
  )

(defun follow-the-sun--tomorrow-in-tz ()
  "Calculate and return tomorrow in the current TZ, decoded."
  (decoded-time-add
   (follow-the-sun--current-time-in-tz)
   (list nil nil nil 1)
   )
  )

(defun follow-the-sun--decoded-time-to-sunrise-date (time)
  "Convert TIME to a date usable by `solar-sunrise-sunset'."
  (let ((day (nth 3 time))
        (month (nth 4 time))
        (year (nth 5 time)))
    (list month day year))
  )

(defun follow-the-sun--sunrise-time (time)
  "Determine the sunrise time for the local date in TIME.

The data is returned as a partial hour, as returned by `solar-sunrise-sunset'."
  (caar (solar-sunrise-sunset (follow-the-sun--decoded-time-to-sunrise-date time)))
  )

(defun follow-the-sun--sunset-time (time)
  "Determine the sunset time for the local date in TIME.

The data is returned as a partial hour, as returned by `solar-sunrise-sunset'."
  (caadr (solar-sunrise-sunset (follow-the-sun--decoded-time-to-sunrise-date time)))
  )

(defun follow-the-sun--is-sun-up ()
  "Determine if the sun is currently up."
  (let* ((current-time-in-tz (follow-the-sun--current-time-in-tz))
         (current-hour-as-cal (+ (caddr current-time-in-tz) (/ (cadr current-time-in-tz) 60.0)))
         (sunrise-timestamp (follow-the-sun--sunrise-time current-time-in-tz))
         (sunset-timestamp (follow-the-sun--sunset-time current-time-in-tz)))
    (< sunrise-timestamp current-hour-as-cal sunset-timestamp)
    )
  )

(defun follow-the-sun--light-mode ()
  "Switch to light mode."
  (load-theme follow-the-sun--light-mode-name t)
  )

(defun follow-the-sun--dark-mode ()
  "Switch to light mode."
  (load-theme follow-the-sun--dark-mode-name t)
  )

(defun follow-the-sun--seconds-until-sun-change ()
  "Return the number of seconds until the next sun change."
  (let* ((current-time-in-tz (follow-the-sun--current-time-in-tz))
         (current-hour-as-cal (+ (caddr current-time-in-tz) (/ (cadr current-time-in-tz) 60.0)))
         (sunrise-timestamp (follow-the-sun--sunrise-time current-time-in-tz))
         (sunset-timestamp (follow-the-sun--sunset-time current-time-in-tz)))
    (1+ (cond
         ((< current-hour-as-cal sunrise-timestamp) (* 60 60 (- sunrise-timestamp current-hour-as-cal)))
         ((< current-hour-as-cal sunset-timestamp) (* 60 60 (- sunset-timestamp current-hour-as-cal)))
         (t (* 60 60 (+ (- 24 current-hour-as-cal) (follow-the-sun--sunrise-time (follow-the-sun--tomorrow-in-tz)))))
         ))
    ))

(defun follow-the-sun--set-correct-theme ()
  "Check the time and set the correct theme."
  (if (follow-the-sun--is-sun-up)
      (follow-the-sun--light-mode)
    (follow-the-sun--dark-mode))
  )

;;;###autoload
(defun follow-the-sun (light-theme dark-theme)
  "Setup follow-the-sun theming with LIGHT-THEME and DARK-THEME."
  (setq
   follow-the-sun--light-mode-name light-theme
   follow-the-sun--dark-mode-name dark-theme
   )
  (follow-the-sun--set-correct-theme)
  (run-with-timer
   (follow-the-sun--seconds-until-sun-change)
   nil
   #'follow-the-sun--set-correct-theme
   )
  )

(provide 'follow-the-sun)
;;; follow-the-sun.el ends here
